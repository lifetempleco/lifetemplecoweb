import React, { useEffect, createRef, useState } from 'react';
import './Header.css';
import logoUrl from '../../assets/images/logo.svg';

const Header: React.FC = () => {
    const location = window.location;
    const isHomePage = location.pathname === '/' || "/thanks";
    const homeLink = createRef<HTMLAnchorElement>();
    const skipToMainLink = createRef<HTMLAnchorElement>();
    const [skipLinkVisible, setSkipLinkVisible] = useState(false);

    useEffect(() => {
        if (isHomePage) {
            homeLink.current?.focus();
        }
    }, [homeLink, isHomePage]);

    return (
        <header>
            <a href="#main-content" className="skip-link" hidden={!skipLinkVisible} onFocus={() => setSkipLinkVisible(true)} onBlur={() => setSkipLinkVisible(false)} ref={skipToMainLink} tabIndex={1}>Skip to main content</a>
            <div className="logo">
                <a href="/" ref={homeLink} autoFocus>
                    <img src={logoUrl} alt="logo" height={150} width={150} />
                </a>
            </div>
        </header>
    );
};

export default Header;
