import React, { useEffect, useRef } from 'react';
import './Footer.css';

const Footer: React.FC = () => {
    //get refs of the links
    const productsLink = useRef<HTMLAnchorElement | null>(null);
    const policiesLink = useRef<HTMLAnchorElement | null>(null);
    const aboutLink = useRef<HTMLAnchorElement | null>(null);
    const contactLink = useRef<HTMLAnchorElement | null>(null);
    const supportLink = useRef<HTMLAnchorElement | null>(null);

    //foucs on the link when on the page
    const location = window.location;
    const isProductsPage = location.pathname === '/products';
    const isPoliciesPage = location.pathname === '/efforts';
    const isAboutPage = location.pathname === '/about';
    const isContactPage = location.pathname === '/contact';
    const isSupportPage = location.pathname === '/donate';

    useEffect(() => {
        if (isProductsPage) {
            productsLink.current?.focus();
        } else if (isPoliciesPage) {
            policiesLink.current?.focus();
        } else if (isAboutPage) {
            aboutLink.current?.focus();
        } else if (isContactPage) {
            contactLink.current?.focus();
        }
        else if (isSupportPage) {
            supportLink.current?.focus();
        }
    }, [isProductsPage, isPoliciesPage, isAboutPage, isContactPage, isSupportPage]);


    return (
        <footer>
            <nav>
                <ul>
                    <li><a href="/about" ref={aboutLink}>About Us</a></li>
                    <li><a href="/products" ref={productsLink}>Products</a></li>
                    <li><a href="/efforts" ref={policiesLink}>Values</a></li>
                    <li><a href="/donate" ref={supportLink}>Donations</a></li>
                    <li><a href="/contact" ref={contactLink}>Contact Us</a></li>
                    {/* Add any other necessary links here */}
                </ul>
            </nav>
            {/* Copyrights Section */}
            <div id='copyrights' style={{ color: "black", fontSize: ".5rem", marginTop: "2rem" }}>
                <p>&copy; {new Date().getFullYear()} LifeTemple Co. All rights reserved.</p>
                <p>Designed by LifeTemple Co.</p>
            </div>
            {/* TODO: Add Term of use and Privacy Policy here */}
            {/* TODO: Add social media links here */}

        </footer>
    );
};

export default Footer;
