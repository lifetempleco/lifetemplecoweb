import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/ Contact';
import Thanks from './pages/Thanks';
import Products from './pages/Products';
import Support from './pages/Support';
import Efforts from './pages/Efforts';


const AppRouter: React.FC = () => {
    return (
        <Router>
            <Routes>
                <Route path="/" Component={Home} />
                <Route path="/about" Component={About} />
                <Route path="/products" Component={Products} />
                <Route path="/efforts" Component={Efforts} />
                <Route path="/donate" Component={Support} />
                <Route path="/contact" Component={Contact} />
                <Route path='/thanks' Component={Thanks} />
            </Routes>
        </Router>
    );
};

export default AppRouter;
