
const Contact = () => {
    return (
        <div>
            <h1>Contact Us</h1>
            <form action="https://formsubmit.co/c14f19640e53ada34ec49ff540cccaf7" method="POST">
                <input type="hidden" name="_subject" value="LifeTempleCo Email" />
                <input type="hidden" name="_template" value="table" />
                <input type="hidden" name="_captcha" value="false" />
                <input type="hidden" name="_next" value="https://www.lifetemple.co/thanks" />
                {/* <input type="hidden" name="_autoresponse" value="Thanks for contacting us!" />
                <input type="hidden" name="_autoresponse_subject" value="Thanks for contacting us!" /> */}
                <label htmlFor="name">
                    Name:
                </label>
                <input type="text" name="name" id="name" required />

                <br />
                <label htmlFor="email">
                    Email:
                    <input type="email" name="email" id="email" required />
                </label>
                <br />
                <label htmlFor="request">
                    Request:
                </label>
                <input type="radio" name="request" id="request" value="question" required />
                <label htmlFor="question">Question</label>
                <input type="radio" name="request" id="request" value="feedback" required />
                <label htmlFor="feedback">Feedback</label>
                <input type="radio" name="request" id="request" value="support" required />
                <label htmlFor="support">Support</label>
                <input type="radio" name="request" id="request" value="other" required />
                <label htmlFor="other">Other</label>
                <br />
                <label htmlFor="message">
                    Message:
                </label>
                <textarea name="message" id="message" required />
                <br />
                <label htmlFor="subscribe">
                    Subscribe to our newsletter
                </label>
                <input type="checkbox" name="subscribe" id="subscribe" />
                <br />
                <button type="submit">Send</button>
            </form>
        </div>
    );
};

export default Contact;
