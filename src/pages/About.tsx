import React from 'react';

const About: React.FC = () => {
    return (
        <div>
            <h1>About Us</h1>
            <p>LifeTempleCo is a non-profit organization that is dedicated to making the world a better place. We are committed to helping people in need, and we believe that everyone deserves a chance to live a happy and fulfilling life.</p>
            <p>Our mission is to provide support and resources to those who need it most, and to help create a world where everyone has the opportunity to thrive.</p>
            <p>Our team is made up of dedicated individuals who are passionate about making a difference. We work tirelessly to help those in need, and we are always looking for new ways to make a positive impact.</p>
            <p>If you are interested in getting involved, we would love to hear from you. Whether you want to volunteer, donate, or simply learn more about what we do, we would be happy to connect with you.</p>
            <p>Together, we can make a difference.</p>

        
        </div>
    );
};

export default About;
