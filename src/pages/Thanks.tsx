import React from 'react';

const Thanks: React.FC = () => {
    return (
        <div>
            <h1>Thank You!</h1>
            <p>Your email submission is complete.</p>
        </div>
    );
};

export default Thanks;
