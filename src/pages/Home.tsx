import React from 'react';

const Home: React.FC = () => {
    return (
        <>
            <h1>LifeTemple Co</h1>
            <p>We're building a web of life. Break the 4D in style.</p>
            <p>Go on.. click through the links below.</p>
            <p>Click the logo to come back here.</p>
        </>
    );
};

export default Home;
