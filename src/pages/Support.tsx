//Support page
import React from 'react';

const Products: React.FC = () => {
    return (
        <>
            <h1>Support Us</h1>
            <p>Thank you for considering to support us. Your support will help us to continue our efforts to make the world a better place.</p>
            <p>There are many ways to support us:</p>
            <ul>
                <li>Donate</li>
                <li>Volunteer</li>
                <li>Spread the word</li>
                <li>Join our team</li>
            </ul>
        </>
    );
};

export default Products;