import './App.css'
import Footer from './components/footer/Footer.js';
import Header from './components/header/Header.tsx';
import animate from "./demo.js"
import ReactRouter from './index.tsx';
function App() {
  animate();
  return (
    <>
      <Header />
      <main id="main-content">
        <ReactRouter />
      </main>
      <Footer />
    </>
  )
}

export default App
