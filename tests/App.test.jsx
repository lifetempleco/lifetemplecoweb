import { chromium } from 'playwright';
import "jest";
import { test, expect } from '@playwright/test';
const deployURL = "https://www.lifetemple.co";
test.describe('App', () => {
    let page;
    let browser;

    test.beforeAll(async () => {
        browser = await chromium.launch();
    });

    test.beforeEach(async () => {
        page = await browser.newPage();
        await page.goto(deployURL);
    });

    test.afterEach(async () => {
        await page.close();
    });

    test.afterAll(async () => {
        await browser.close();
    });

    test('should display the title correctly', async () => {
        const titleElement = await page.$('title');
        const titleText = await titleElement.innerText();
        expect(titleText).toBe('LifeTemple Co.');
    });

    test('should navigate to the about page when clicking the about link', async () => {
        const aboutLink = await page.$('a[href="/about"]');
        await aboutLink.click();
        const currentUrl = page.url();
        expect(currentUrl).toBe(deployURL + '/about');
    });
});
